const express = require("express");
const bodyParser = require("body-parser"); // Middleware for parsing JSON in the request body
const fs = require("fs");
const app = express();
const cors = require("cors");

// const data = require('./data.json');

app.use(cors());

const sqlite3 = require("sqlite3").verbose();
const filepath = "./ble.db";

function createDbConnection() {
  const db = new sqlite3.Database(filepath, (error) => {
    if (error) {
      return console.error(error.message);
    }
    createTable(db);
  });
  console.log("Connection with SQLite has been established");
  return db;
}

function createTable(db) {
  db.get(
    "SELECT name FROM sqlite_master WHERE type='table' AND name='bridge_logs'",
    (err, row) => {
      if (err) {
        console.error(err.message);
        return;
      }

      if (!row) {
        // Table does not exist, so create it
        db.exec(`
        CREATE TABLE bridge_logs
        (
          ID INTEGER PRIMARY KEY AUTOINCREMENT,
          name   VARCHAR(50) NOT NULL,
          rssi   VARCHAR(50) NOT NULL,
          distance INTEGER NOT NULL,
          created_at VARCHAR(50) NOT NULL
        );
      `);
        console.log("Table 'bridge_logs' has been created.");
      } else {
        console.log("Table 'bridge_logs' already exists.");
      }
    }
  );
}

const db = createDbConnection();

// const { PrismaClient } = require("@prisma/client");
// const { PassThrough } = require("stream");
// const prisma = new PrismaClient();
const port = 4002; // Choose any available port
const jsonArray = [];

// async function saveData(inc_data) {
//   console.log("inc data", inc_data);
//   const user = await prisma.device_data.create({
//     data: {
//       inc_data,
//     },
//   });
//   console.log(user);
// }

function isValidJSON(jsonString) {
  try {
    JSON.parse(jsonString);
    return true;
  } catch (error) {
    return false;
  }
}

app.get("/download", (req, res) => {
  const jsonString =
    '{"name":"John Doe","age":30,"isStudent":false,"grades":[90,85,92],"address":{"city":"New York","state":"NY","zipCode":"10001"}}';

  // Parse the JSON string to a JavaScript object
  const jsonObject = JSON.parse(jsonString);

  // Convert JSON to CSV format
  const csvString = jsonToCsv(jsonObject);

  // Set headers for CSV file download
  res.setHeader("Content-disposition", "attachment; filename=data.csv");
  res.setHeader("Content-type", "text/csv");

  // Send the CSV content as a file
  res.send(csvString);

  // Optionally, you can also save the CSV content to a file on the server
  // fs.writeFileSync('data.csv', csvString);
});

// Middleware to parse JSON in the request body
app.use(bodyParser.json());

// POST endpoint
app.post("/upload", (req, res) => {
  // Assuming the request body contains JSON data
  const jsonData = req.body;

  // Do something with the JSON data (e.g., log it)
  console.log("Received JSON data from server:", jsonData);

  //const { distance, rssi, name } = jsonData;

  // db.run(
  //   "INSERT INTO sharks (name, rssi, distance) VALUES (?, ?, ?)",
  //   [name, rssi, distance],
  //   function (err) {
  //     if (err) {
  //       console.error(err.message);
  //       return res.status(500).json({ error: "Internal server error." });
  //     }

  //     // Successfully inserted data
  //     res.status(200).json({ message: "Data added to sharks table." });
  //   }
  // );

  if (jsonData != null) {
    console.log("json data", jsonData);
    const jsonString = JSON.stringify(jsonData);

    // Append the JSON data to an existing file or create a new file if it doesn't exist
    fs.appendFileSync("data.json", jsonString + "\n", "utf-8");
  }

  // Send a response
  res.json({ message: "JSON data received successfully to the server" });
});

// ;





app.post('/add-logs', (req, res) => {
  const jsonData = req.body;

  console.log("Received JSON data:-", jsonData);

  // Validate input data
  if (!jsonData || !jsonData.distance || !jsonData.rssi || !jsonData.name) {
    return res.status(400).json({ error: 'Missing required fields.' });
  }

  const { distance, rssi, name, created_at } = jsonData;

  // Get the current timestamp
  //const createdAt = new Date().toLocaleString();

  // Insert data into 'sharks' table with created_at column
  db.run(
    'INSERT INTO bridge_logs (name, rssi, distance, created_at) VALUES (?, ?, ?, ?)',
    [name, rssi, distance, created_at],
    function (err) {
      if (err) {
        console.error(err.message);
        return res.status(500).json({ error: 'Internal server error.' });
      }

      // Successfully inserted data
      res.status(200).json({ message: 'Data added successfully.' });
    }
  );
});


app.get('/retrieveData', (req, res) => {
  // Example query to retrieve data
  const query = 'SELECT * FROM bridge_logs';

  // Execute the query
  db.all(query, (err, rows) => {
    if (err) {
      console.error(err.message);
      res.status(500).send('Internal Server Error');
    } else {
      // Send the retrieved data as a JSON response
      res.json(rows);
    }
  });
});


app.get("/alldata", (req, res) => {
  fs.readFile("data.json", "utf8", (err, data) => {
    if (err) {
      console.error("Error reading the file:", err);
      return;
    }

    try {
      const resultArray = data.split(/\r?\n/);
      const arrayOfObjects = [];
      resultArray.forEach((element) => {
        if (isValidJSON(element)) {
          const parsedObj = JSON.parse(element);
          //console.log("parse", parsedObj);
          arrayOfObjects.push(parsedObj);
        }
      });

      const sortedArrayDescending = [...arrayOfObjects].sort((a, b) => {
        const timeA = new Date("1970-01-01 " + a.timestamp);
        const timeB = new Date("1970-01-01 " + b.timestamp);
        return timeB - timeA;
      });

      // console.log(sortedArrayDescending);

      console.log("sorted of objects", sortedArrayDescending);
      res.json(sortedArrayDescending);
    } catch (parseError) {
      console.error("Error parsing JSON:", parseError);
    }
  });
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

// Helper function to convert JSON to CSV
function jsonToCsv(json) {
  const header = Object.keys(json).join(",");
  const values = Object.values(json).join(",");
  return `${header}\n${values}`;
}
