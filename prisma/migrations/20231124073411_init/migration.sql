/*
  Warnings:

  - You are about to drop the column `rss` on the `device_data` table. All the data in the column will be lost.
  - Added the required column `rssi` to the `device_data` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_device_data" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "serviceUUID" TEXT NOT NULL,
    "timestamp" TEXT NOT NULL,
    "rssi" REAL NOT NULL,
    "distance" REAL NOT NULL
);
INSERT INTO "new_device_data" ("distance", "id", "serviceUUID", "timestamp") SELECT "distance", "id", "serviceUUID", "timestamp" FROM "device_data";
DROP TABLE "device_data";
ALTER TABLE "new_device_data" RENAME TO "device_data";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
